﻿using Eonix.Monkeys.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eonix.Monkeys.Models
{
    public class Spectator
    {
        public string Name { get;}

        public Spectator(string name)
        {
            Name = name;
        }
        public void ReactToTrick(Trick trick, string nameOfMonkey)
        {
            switch (trick.TrickType)
            {
                case TrickType.Acrobatics:
                    Applaud(trick, nameOfMonkey);
                    break;
                case TrickType.Music:
                    Whistle(trick, nameOfMonkey);
                    break;
            }
        }

        private void Applaud(Trick trick, string nameOfMonkey)
        {
            Console.WriteLine($"{Name} applaudit pendant le tour d'acrobatie '{trick.Name}' de {nameOfMonkey}");
        }

        private void Whistle(Trick trick, string nameOfMonkey)
        {
            Console.WriteLine($"{Name} siffle pendant le tour de musique '{trick.Name}' de {nameOfMonkey}");
        }
    }
}
