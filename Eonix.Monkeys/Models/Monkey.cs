﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Eonix.Monkeys.Models
{
    public class Monkey
    {
        public string Name { get;}
        public event Action<Trick,string> OnTrickExecuted;

        public List<Trick> Tricks { get;}

        public Monkey(string name, List<Trick> tricks)
        {
            Name = name;
            Tricks = tricks;
        }

        public void ExecuteTrick(Trick trick)
        {
            if(!Tricks.Any(t => t.Equals(trick)))
                return;
            Console.WriteLine($"{Name} exécute le tour {trick.Name}");
            OnTrickExecuted?.Invoke(trick, Name);
        }
    }
}
