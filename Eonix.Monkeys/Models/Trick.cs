﻿using Eonix.Monkeys.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eonix.Monkeys.Models
{
    public struct Trick
    {
        public string Name { get; set; }
        public TrickType TrickType { get; set; }
    }
}
