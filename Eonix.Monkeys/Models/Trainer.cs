﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eonix.Monkeys.Models
{
    public class Trainer
    {
        private readonly Monkey _monkey;
        public Trainer(Monkey monkey)
        {
            _monkey = monkey;
        }

        public void PerformTheMonkeyTricks()
        {
            foreach (Trick trick in _monkey.Tricks)
            {
                _monkey.ExecuteTrick(trick);
            }
        }
    }
}
