﻿using Eonix.Monkeys.Enums;
using Eonix.Monkeys.Models;
using System;
using System.Collections.Generic;

namespace Eonix.Monkeys
{
    class Program
    {
        static void Main(string[] args)
        {
            Spectator spectator = new Spectator("Victor");

            Monkey monkey1 = new Monkey("Singe 1", new List<Trick>() 
            {
                new Trick()
                {
                    Name = "Marcher sur les mains",
                    TrickType = TrickType.Acrobatics
                },
                new Trick()
                {
                    Name = "Jouer de la flute",
                    TrickType = TrickType.Music
                }
            });

            Monkey monkey2 = new Monkey("Singe 2", new List<Trick>()
            {
                new Trick()
                {
                    Name = "Tourner sur la tête",
                    TrickType = TrickType.Acrobatics
                },
                new Trick()
                {
                    Name = "Jouer de la guitare",
                    TrickType = TrickType.Music
                }
            });

            monkey1.OnTrickExecuted += spectator.ReactToTrick;
            monkey2.OnTrickExecuted += spectator.ReactToTrick;

            Trainer trainer1 = new Trainer(monkey1);
            Trainer trainer2 = new Trainer(monkey2);

            trainer1.PerformTheMonkeyTricks();
            trainer2.PerformTheMonkeyTricks();

            monkey1.OnTrickExecuted -= spectator.ReactToTrick;
            monkey2.OnTrickExecuted -= spectator.ReactToTrick;
        }
    }
}
